#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Xecestel'
SITENAME = 'Xecestel Games'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ('Itch.io', 'https://xecestel.itch.io'),
    ('My GitLab', 'http://gitlab.com/xecestel'),
)

# Social widget
SOCIAL = (
    ('Twitter', 'https://twitter.com/xecestel'),
)

DEFAULT_PAGINATION = 10

THEME = './themes/pelican-blueidea'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
