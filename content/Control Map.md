Title: Control Map
Date: 2019-08-13 20:14
Modified: 2019-08-13
Category: Other projects
Tags: script, godot, godot 3.1
<!--Slug: my-super-post
Authors: Alexis Metaireau, Conan Doyle-->
Status: draft
Summary: Some informations about my script called *Control Map*

<a href="https://gitlab.com/xecestel/control-map-script" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/1/18/GitLab_Logo.svg"><br>Check it out on GitLab!</a>

#Curretion version
Version 0.9.5

#About the script
When I first released Like a Cat Out of Hell! for the ninth edition of the Godot Wild Jam I soon realized that the main problem was the input mapping I used. It was fine for me and the friends who I asked to test the game, but not that good for some people who tried after the release. Ghosting problem, lack of clarity in the tutorial, the mapping had to be redone.

When the game jam finished and I had the go-ahead to update the title, the first things I made were a scene and a script to customize the controls of the game. Unfortunately, I had to face a terrible truth: the default Input Map of Godot is a mess. I checked everything without finding a single method to customize the mapping, so I had to do it directly through code, but it wasn't easy to work with how it's implemented in the engine and the final result was... Disappointing, to say the least.

I knew I had to make a better version in the future, and so I did.

The easiest way you can customize the input map is by hard coding everything. Instead of using the "actions" the engine lets you create, you make press checks on the single keys. Obviosyly this is quite dangerous, as every change has to be notified in every script is needed.

That's why I made this script. This is technically hard coding, but way more safe and easy to use.

The script basically replace every methods on the Input classes of the engine. All of them. You know, before starting to learn Godot I programmed a little bit with the good old RPG Maker, not only games but also scripts (RPG Maker VX Ace) and plugins (RPG Maker MV), so I obviously inherited that mindset: the script had to be as plug and play as possible, even for dummies if I had the possibility.

And here we have it. I made my very own input map that comes with lots of methods for easy customization. The only note I could make is that I'm afraid the code is a little slower than the default code for the input. I never experienced input lag while testing (and the laptop I used to make it is basically a toaster) so I don't think it will a problem, at least not for 2D games.

#Current features
- Plug and play:
The only thing you have to remember is that you need to use this input map and not the default one. This just means that instead of using `Input.is_action_pressed` you can use `ControlMap.is_action_pressed`. Take a look at the instructions to see which method are currently available and how to use them.
- Easy configuration:
The input maps use just strings with name of actions and buttons. This makes this script very easy to configure, and also to edit as needed. With all the getters and setters in the code you can also program a remapping feature as simple as it should be!
- Make the input map you always dreamed of:
you want to make your player move with the mouse buttons? Or maybe you want to program a double command like Mouse Button + Left Arrow Key? And why not both? Following the instructions in the README file you can easily do that!
- Two customizable input maps for mouse+keyboard and joypad:
just follow the instructions to add and remove actions and you can make your input map as you wish!
- Skeleton method for an easy data saving implementation:
uncomment the method and edit as you need if you want to implement this feature in your game. No other scripts required.

#How does it work
## Configuration
In order to work, it uses 6 dictionaries: 2 variable and 4 constant. The 4 constant dictionaries are used to make every button and key codes to an alphabetic string for easier usage of the script. You don't need to touch them (and I don't advise you to if you don't know what you're doing), but you can check them if you want to know what names you can use for the buttons.
What do you need them for?
To populate the mapping dictionaries.
There's two of them: one for the keyboard and mouse and one for the joypad. Feel free to add and remove all the actions you want, just be sure to make every one of them correspond to a key or button and that this key or button exists in the other dictionaries.
The values in the Dictionaries are not just strings: they are Arrays of Array of Strings. This means that you can also add multiple buttons per action and a modifier for every button. You can find the maps under the "CONFIGURATION" category in the script.
So, let's assume you want to make the player go left with the Left key and by pressing Shift and D, for some reason. To do that you will have to make an entry in the KeyBoardMap Dictionary like this:
```
"left"	:	[ ["Left"], ["Shift, "D"] ]
```

There's also a commented method that lets you sync the mapping with some other AutoLoad script for saving game purposes. You can fine more instructions there.

##Notable methods
Now, here`s a list of all the methods you can use in your code and how they work:
- `ControlMap.is_action_pressed(action : String) -> bool`:
This method replaces `Input.is_action_pressed method`. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). It returns a boolean value that tells you if the actions has being pressed.
- `ControlMap.is_action_just_pressed(action : String) -> bool`:
This method replaces `Input.is_action_just_pressed` method. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). It returns a boolean value that tells you if the actions has being just pressed.
- `ControlMap.is_event_pressed(action : String, event : InputEvent) -> bool`:
This method works with the `_input(event)` method. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). `event` is an `InputEvent` and it's basically the argument of the `_input(event)` method. This method returns a boolean value that tells you if the event pressed corresponds to the given action.
- `ControlMap.is_event_just_pressed(action : String, event : InputEvent) -> bool`:
This method works with the `_input(event)` method. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). `event` is an InputEvent and it's basically the argument of the `_input(event)` method.
This method it's similar to the `ControlMap.is_event_pressed` method but with a `just_pressed` logic added.
- `ControlMap.is_action_released(action : String) -> bool`:
This method replaces Input.is_action_just_released method. `action` is a string and has to correspond to one of the actions in the mapping dictionaries (see more informations above). It returns a boolean value that tells you if the actions has being just released.


The script also comes with additional useful methods:
- `ControlMap.get_action_button_indexes(action)`:
This method returns an Array with the mapped joypad buttons indexes of an action.
- `ControlMap.get_action_key_scancodes(action)`:
This method returns an Array with the mapped keyboard buttons scancodes of an action.
- `ControlMap.get_event_button_name(event)`:
Given an `InputEvent` (like the one you get from `_input(event)` method), it can tell you the name of the button it corresponds to.
- `ControlMap.get_action(event)`:
Given an `InputEvent` (like the one you get from `_input(event)` method), it can tell you what action it corresponds to.

#How to install
To install this script in your project just clone the git repository in your file system and copy-paste the script (along with the License file) in your project directory.

I strongly advise not to clone the git repository directly into your project: this way it will be easier to download potential updates. If I release an update you can just download it in the external folder and then copy paste the internal code leaving the Configuration section safe. Easier and safier.

#Future roadmap
<!--TO DO-->

#Source code
You can find the source code for this script in the <a href="https://gitlab.com/Xecestel/control-map-script" target="_blank">GitLab repository</a>.

#Comments
If you have issues, comments, questions about the script or the source code, you can contact me by writing a comment on the itch.io page or by opening an issue ticket on the GitLab page.

#Copyright
Control Map Script

Copyright (C) 2019  Celeste Privitera

The Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with the file, You can obtain one at [https://mozilla.org/MPL/2.0/](https://mozilla.org/MPL/2.0/).
