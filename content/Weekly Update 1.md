Title: Weekly Updates! #1: on a new project, control mapping and broken hard disks
Date: 2019-08-13 16:36
Modified: 2019-08-13 16:36
Category: Devlogs
Tags: weekly update, godot, new project, game
Slug: weekly-update-1
<!--Authors: Alexis Metaireau, Conan Doyle-->
Summary: New weekly update about my current projects and what I did lately.

# The accident
So, where to begin?
I swear, I seriously wanted to publish this post last week. I even wrote a draft! Unfortunately I hadn't pushed it on the git repository before the... accident happened.

I dropped my laptop while playing with my cat. I tripped on the charger cable and the the laptop fell on the floor. At first I thought it was actually okay (it was on at the time), but then I realized I couldn't open any program or folder. I tried to reboot the system just to be told by a black screen that the hard disk was unreadable.

So you see, children? Always push your stuff on git before turning off your computer for the day.

Luckily I didn't lose much. I lost this post (but nevermind, I can always write it again), and some work in progress project files I was working on that I didn't backup because I'm scientifically and idiot. Luckily, Amazon is pretty fast with its deliveries, so I already replaced the hard disk (with a way better SSD, to look at the bright side on this story) and I'm back in business!

Anyhow, this means that this update will have a little less news than the first lost draft. I wanted to show you something I was working on, but, as I said, I lost the work in progress files.

But I can talk about something else, if you want.

# The new project
As I say every time I talk about [50 Years Later](https://xecestel.gitlab.io/50-years-later.html) when I finished the development of that game, I was actually working on another project.

And I still am.

I cannot tell you much about it at its current state, as it's still in development. The code name I will use when referring to it will be **Bread Game**. It will be an arcade platformer and I don't know when it will be released yet. I think I can safely say that this is the most ambitious game I worked on since I started learning Godot.

So it will take some time, you know.

At the moment, by the way, I almost made all the needed assets and all the needed scripts, so I think I can say I'm about at the 40% of the work. More or less. I can't make promises, but I'm planning to release the game as an early access with a singleplayer mode on GitLab and Itch.io by the end of the summer. I think I can make it.

Yea, it's not some kind of weird typo: "_early access with a singleplayer mode_". I'm making this game to familiarize with Godot's random number generator and with the multiplayer modes, both local and online. I seriously think (and obviously hope) you will have a lot of fun with this project.

Stay tuned, then, because every week I will use this space to level you on the updates.

# What I did lately
I seriously wanted to show you the Tilesets I was working on last week. Unfortunately I lost both of them. To look at the bright side, I wasn't fully satisfied and they seriously needed to be polished, so it's not _that_ bad. I think. But do you know what I can tell you about? Code!

## Control Map
When I published the first release of [Like a Cat Out of Hell!](https://xecestel.gitlab.io/like-a-cat-out-of-hell] at the end of the game jam most of the issues on the comments were about the controls. Some players experienced ghosting problems on their keyboards. I promised I would make ammend when the jam would be over and release an update with a control customization feature.

And I did, it was version 1.1 (the script was completed in version 1.1.1, tho). But it actually did nothing to help those players. You see, Godot 3.1 doesn't have methods to edit the default Input Map. You can only work on it directly, hard coding everything. But the default Input Map is... A mess, really.

You have a lot of codes, numbers, InputEvent objects, arrays, dictionaries, you have no idea how many `print` I had to call just to try to understand how it worked. My script was a mess, it was hard to read and even harder to write and mantain and at the end it didn't even do what it was supposed to. I was really not satisfied with it, but I didn't know how to make it better.

In 50 Years Later I didn't add the control customization, but I wanted to did it for Bread Game. So I went to the Like a Cat project directory and copy-pasted the script on my new project. You have no idea I much hard was to adapt the code to the new game and controls.

It's a that moment I realized it was time to make a better script. So I started thinking on a way I could stop rely on the default Input Map without the need to hard code every single input event.

And the answer was: I had to make my very own Input Map. And its name is Control Map. And you can find it [here](https://gitlab.com/xecestel/control-map-script).

So how does it work? The idea was to make a script as plug and play as possible. To achieve that I made my own input map, using two dictionaries (one for the joypad and one for keyboard and mouse) that used just strings. One string for the action, pretty much identical to the ones you would use for the default Input Map, the other for the name of the button you are mapping. This way, working with this script is waaaay easier than working with the Input Map. Then I replaced all the basics methods of the Input, like `is_action_pressed`, `is_action_just_pressed`, `is_event_pressed` and so on, and most of them are identical to the default version. You just have to use, for example, `ControlMap.is_action_pressed("ui_left")` instead of `Input.is_action_pressed("ui_left")` and it does all the work with you. But this script also comes with brand new getters and setters that let you edit and check every action and command on the input maps with just a line of code.

At its current state (Version 0.9.5) the script has basically all the features and methods you should need, so you can already try to familiarize with it if you want to use it (it's a free software). The problem is: I don't have a joypad I can plug on my laptop at the moment, so I'm not entirely sure the joypad input works as intended (and I have some doubts on the Joypad Motion part). If you want to give me an hand to complete this project, feel free to do, and I will most certainly add you in some credits text I will write.

Please, note that this script is intended to work for Godot Engine Version 3.1. When new releases of the engine will come out, some methods, and variables, and stuff could change. I will update the script accordingly, but as it's possible that the said new version of the engine already provides methods to customize the Input Map by code, check the official documentation to make sure you actually still need this script.

I will try to make you know if the script is no more needed, should it happen.

I will talk some more about this script in some future post, so let's end this here for the moment. If you are not quite sure how the script works wait for these posts, and if they're not enough, wait for the release of Bread Game source code as it will be the first time I will use this script for an actual project, so you will be able to see an example of use.

# Thanks for reading

I think I talked enough for this weekly update. I will give you more insight about Bread Game next time and I hope I will be able to show you something more (I'm really sorry for the accident).

Thank you for taking the time to read this post, please check out the My games section or my itch.io profile to take a loot at my games, or follow me on Twitter to get more insight about my work (and some random stuff too).

© Xecestel
