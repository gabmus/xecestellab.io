Title: 50 Years Later
Date: 2019-08-04 15:17
Modified: 2019-08-04 15:17
Category: My games
Tags: linux, windows, browser, games, godot
<!--Slug: my-super-post
Authors: Alexis Metaireau, Conan Doyle-->
Summary: Some informations about my game called *50 Years Later*

<div style="font-family: sans; background-color: white; width: 600px; height: 170; border: 1px solid #dadada; display: flex;"><div style="border-right: 1px solid #dadada;"><img style="margin: 12px 12px 12px 12px; box-shadow: 0 10px 10px -10px rgba(0,0,0,0.3); display: block !important;" src="https://img.itch.zone/aW1nLzIzMzQ2MjkucG5n/180x143%23c/5dQb81.png"/></div><div stlye="display: flex; flex-direction: column;"><div style="display: flex; margin: 25px 0 0 0;"><h1 style="font-weight: bold; margin: 0 10px 10px 15px; font-size: 25px;">
                50 Years Later
            </h1><svg xmlns="http://www.w3.org/2000/svg" style="fill: #888888;" width="24" height="24" version="1.1" viewBox="0 0 24 24"><text x="-2.1353383" y="7.6818104" fill="#000000" font-family="sans-serif" font-size="40px" letter-spacing="0px" word-spacing="0px" style="line-height:1.25" xml:space="preserve"><tspan x="-2.1353383" y="7.6818104"/></text><path d="m10.849 3.04c-1.4078 0.087178-2.988 0.093245-4.1383 1.0208-1.1753 1.0319-1.9923 2.4912-2.0907 4.0693-0.2647 2.763 0.17961 5.5196 0.41552 8.2682-0.088859 1.7947-1.3316 3.2138-2.4196 4.5322 0.7844 0.06131 1.5786 0.01512 2.3675 0.02956 0.39042-0.88916 1.0905-1.6717 1.2556-2.6407 0.20125-1.0577 1.1013 0.65425 1.6917 0.74168 1.8583 0.54862 3.7636-0.35661 5.2064-1.4929 0.47097-0.45489 1.2809-1.1389 1.3793-0.04283 0.41363 1.1031 0.98961 2.1315 1.5228 3.1753 0.31248 0.47118 0.90924 0.18945 1.3735 0.25943h3.996c-0.91366-1.2274-1.8705-2.4565-2.4367-3.8889-0.70708-1.1352-0.69971-2.529-0.92143-3.8082-0.08435-1.2968-0.28131-2.5772-0.3806-3.8723-0.235-1.7342-0.7298-3.6222-2.197-4.7279-1.2107-1.1288-2.9917-1.6554-4.6241-1.6227zm1.5617 6.558c1.2568-0.23228 2.072 1.128 1.884 2.2444-0.06307 0.68852 0.0095 2.0413-0.3952 2.2862-0.45041-0.1485-1.1456-0.3331-0.35518-0.61346 0.80269-0.87538 0.27245-2.7259-1.01-2.724-1.2247 0.11638-1.1611 1.5106-1.1467 2.3723-0.88761-0.67689-0.96932-2.3328-0.22745-3.1903 0.35358-0.27107 0.80681-0.4032 1.2506-0.37519zm-5.4965 0.25012c1.2628 0.037686 1.5684 1.5555 1.4386 2.5647-0.3104 1.2335-0.26881-0.54087-0.50934-0.95727-0.38427-0.93061-1.7886-0.5531-1.8046 0.40017-0.013614 0.67416-0.095078 1.6431 0.68626 1.9272-0.94515 0.67104-1.2633-1.1972-1.1811-1.8656 0.019074-0.81772 0.37016-2.0517 1.3702-2.0692zm0 1.3116c0.87509-0.27642 0.63529 1.9668 0.40137 0.70916 0.037624-0.46913-1.0851-0.11894-0.40137-0.70916zm5.9327 0c0.80743-0.0509 0.83929 1.9127 0.20226 0.6985-0.1157-0.36332-0.90608-0.52103-0.20226-0.6985zm-3.4345 1.8118c1.4219 0.06244 2.6981 0.76304 4.0282 1.1899 0.72891 0.25634 1.8673 1.0151 1.0326 1.8185-0.70182 0.64438-1.5954 1.0393-2.3435 1.6307-1.1194 0.75519-2.5621 1.5624-3.9239 0.95295-0.921-0.48446-1.811-1.2339-2.2631-2.1685 0.72446 0.4973 1.5012 1.0102 2.4148 1.0553 1.5967 0.14346 3.0006-0.75552 4.3514-1.4639 0.62209-0.10538 0.66066-1.2375-0.01561-0.56543-0.78605 0.72608-1.7927 1.187-2.8097 1.5024-1.4151 0.45533-3.0672 0.0076-3.9978-1.1619-0.4679-0.8113 0.6049-1.3916 1.1243-1.8392 0.71426-0.481 1.5366-0.87255 2.4024-0.95091zm-0.62529 0.18606c-0.67987 0.04352-0.8054 1.1527-0.052062 0.56882 0.17042-0.12328 0.35838-0.46844 0.052062-0.56882zm0.87541 0c-0.58529 0.53271 0.95704 1.1535 0.43439 0.32845-0.12236-0.13487-0.26878-0.25131-0.43439-0.32845z"/></svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill: #888888;" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M3,12V6.75L9,5.43V11.91L3,12M20,3V11.75L10,11.9V5.21L20,3M3,13L9,13.09V19.9L3,18.75V13M20,13.25V22L10,20.09V13.1L20,13.25Z"/></svg><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill: #888888;" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="M12,17.56L16.07,16.43L16.62,10.33H9.38L9.2,8.3H16.8L17,6.31H7L7.56,12.32H14.45L14.22,14.9L12,15.5L9.78,14.9L9.64,13.24H7.64L7.93,16.43L12,17.56M4.07,3H19.93L18.5,19.2L12,21L5.5,19.2L4.07,3Z"/></svg></div><div style="margin-left: 15px; font-size: 12px;">
            by <a style="font-weight: bold;" href="https://xecestel.itch.io">Xecestel</a>
        </div><div style="margin-left: 15px; margin-top: 6px; font-size: 16px;">
            2D Pixel Art Casual Arcade game
        </div><a target="_blank" style="text-decoration: none; padding: 0 !important;" href="https://xecestel.itch.io/50-years-later"><div style="font-size: 14px; margin: 12px 0 24px 15px;  padding: 8px; text-align: center; width: 110px; background-color: #FA5C5C; color: white; border: 1px solid #E15353; border-radius: 4px;">
            Play on <strong>itch.io</strong>
        </div></a></div></div>

#About the game
I made the first level of this game in less than 48 hours as a tribute for the fiftieth anniversary of the Moon Landing. Then, weeks later, I decided to add some levels and publish it as a simple browser game.<br>
In this game the player controls the NASA lunar lander as it's trying to land on the moon surface. Your objective is to land on the right spot on the moon (the red circle) without crashing on walls, on the floor or on the asteroids flying on the screen.<br>

#Currently supported platforms
- Browser (currently tested on Firefox and Google Chrome)
- Linux 64 bit
- Windows 32 bit
- Windows 64 bit<br>

#How to play
- Press the Right and Left keys on your keyboard to rotate the lander.
- Hold Spacebar to fire the propellers
- Pay attention to the gasoline level on the top-left corner of the screen: if it goes down to zero, you will not be able to fly anymore!
- Get to the red circle on the ground without crasing against the floor, the walls or the asteroids.
- Press R to restart a level.
- Press F or the button on the screen to make the game fullscreen, and press M or the button on the screen to mute or unmute all the game sounds.<br>

#How to install
You can just play it on the itch.io page. If you want to install it locally, anyway, follow these instructions.<br>
Download the right version from the itch.io page according to your operating system and/or your system architecture. Export the compressed archive in a directory of your choice, making sure that the .pkg and the executable are on the same folder.
Just double click on the executable and enjoy the game!<br>

#Source code
You can find the source code for this game in the <a href="https://gitlab.com/Xecestel/50-years-later" target="_blank">GitLab repository</a>.<br>

#Comments
If you have issues, comments, questions about the game or the source code, you can contact me by writing a comment on the itch.io page or by opening an issue ticket on the GitLab page.<br>

#Screenshots
There are currently no screenshot available for this game.<br>

#Copyright
50 Years Later<br>
Copyright (C) 2019  Celeste Privitera<br>
This game is released under the GNU GPL3 (General Public License 3) license.<br>
For more informations, please read the README file on the GitLab repository under the License paragraph.
