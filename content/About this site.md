Title: About me, about all of this
Date: 2019-08-03 21:02
Modified: 2019-08-04 14:25
Category: About
Tags: info
<!--Slug: my-super-post
Authors: Alexis Metaireau, Conan Doyle-->
Summary: Useful informations about me and this site.

#About me
Hey guys! Welcome to my personal website!<br>
My name is Celeste, but you can call me Xecestel.<br>
I'm a wannabe game designer from Italy trying her best to make other people have fun.
I was born in Sicily and currently I live in Turin, where I study Computer Science. I'm currently 24 years old.<br>

#About the software I use
To make the 2D Sprite I use in my games I just use GIMP. It's not perfect, but it's more then enough to make pixel-art.<br>
To make the code I usually use Godot Engine, because it's open source and has a good support for Linux systems.<br>
The games you will find here will all be open source. You can find the source codes on my <a href="https://gitlab.com/Xecestel/" target="_blank">GitLab</a>! Every feedback, bug report, advise will be appreciated!<br>

#About my games
I like writing. I really like it. I would love to make a story driven game one day. Unfortunately I'm not that good with programming and game arts yet.<br>
I like to make games which are simple but with peculiar mechanics and settings, and I hope that other people find them fun as I do!<br>
If you want to see more about my games, you can check my profile here on itch.io or my GitLab page.<br>

#About this blog
What I'll be doing here?<br>
This site is meant to be seen as some kind of... showcase? If you like. Here you will find links, changelogs and other useful informations about all the game I will make and publish on my <a href="https://xecestel.itch.io" target="_blank">itch.io page</a>.<br>
"*Can't I just follow you on itch.io, then?*", I can see you asking. Well, of course you can, and I would very much appreciate if you do! But I want this blog to be something more.
First of all, you can find everything here without needing to make an itch.io account, if you don't have one. Second thing, this is also a devlog, or a dev diary, or whatever you'd like to call it. I will post frequent posts and updates not only about games I already published, but also about work in progress projects!<br>
So if checking my itch.io page you like what you can see and play, well, I would love if you'd follow me also in here!<br>

#Thanks, conclusions and miscellaneous
I hope to learn many things during my time here, and I obviously hope we will have fun together!<br>
Thank you for reading this and many thanks for playing or even checking my games.<br>
And remember: every feedback is well accepted!<br>

Other instructions:<br>

- If you want to support my work, consider leaving a donation before or after downloading one of my games. Even 1$ would be more than appreciated!
- If you want to help me with my projects by making code, art, music or with the localization, leave a comment on the page of the game you want to help with or just email me at the address below.
- If you want to work with me on some project, just contact me at the email address below.<br>

 Email: <a href="mailto:xecestel@gmail.com">xecestel@gmail.com</a>
